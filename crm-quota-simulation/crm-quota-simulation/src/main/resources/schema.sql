drop table if exists quota;

create table quota (
  id integer primary key,
  subscriberid varchar(100),
  freequota integer,
  usedquota integer,
  expirydate date
);

drop table if exists subscribers;

create table subscribers (
  serial varchar(100) primary key,
  email varchar(100),
  secretcode varchar(100)
  
  );
  
drop table if exists AppDetails;

create table AppDetails (
  appid varchar(100),
  secretcode varchar(100),
  serial varchar(100),
  PRIMARY KEY (serial,appid)


);

drop table if exists ConnectedDevices;

create table ConnectedDevices (
  serial varchar(100),
  mac varchar(32000),
  PRIMARY KEY (serial)

);

