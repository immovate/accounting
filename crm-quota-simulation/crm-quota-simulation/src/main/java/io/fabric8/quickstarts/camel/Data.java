package io.fabric8.quickstarts.camel;

public class Data {
	 private String message;
	 private String title;


	 // Getter Methods 

	 public String getMessage() {
	  return message;
	 }

	 // Setter Methods 

	 public void setMessage(String message) {
	  this.message = message;
	 }
	 
	 public String getTitle() {
		  return title;
		 }

		 // Setter Methods 

		 public void setTitle(String title) {
		  this.title = title;
		 }
	}