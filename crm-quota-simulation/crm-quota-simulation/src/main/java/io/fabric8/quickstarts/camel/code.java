/*
 * Copyright 2005-2016 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version
 * 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
package io.fabric8.quickstarts.camel;

import java.util.Date;

public class code {

    private String serial;
    private String secretcode;
    private String email;
   
    
    public String getemail() {
		return email;
	}
	public void setemail(String email) {
		this.email = email;
	}
	
	
	public String getserial() {
		return serial;
	}
	public void setserial(String serial) {
		this.serial = serial;
	}
	public String getsecretcode() {
		return secretcode;
	}
	public void setsecretcode(String secretcode) {
		this.secretcode = secretcode;
	}

    
}
