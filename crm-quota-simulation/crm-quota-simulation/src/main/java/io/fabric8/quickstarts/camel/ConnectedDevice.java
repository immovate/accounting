package io.fabric8.quickstarts.camel;



public class ConnectedDevice {
	
		 private String name;
		 private String macaddress;


		 // Getter Methods 

		 public String getName() {
		  return name;
		 }

		 public String getMacaddress() {
		  return macaddress;
		 }

		 // Setter Methods 

		 public void setName(String name) {
		  this.name = name;
		 }

		 public void setMacaddress(String macaddress) {
		  this.macaddress = macaddress;
		 }
		 
}