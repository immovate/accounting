package io.fabric8.quickstarts.camel;

import java.util.Date;


public class push {
 private String title;
 private String badge;
 private String sound;
 private String body;
 Data DataObject;
 private String to;


 // Getter Methods 

 public String getTitle() {
  return title;
 }

 public String getBadge() {
  return badge;
 }

 public String getSound() {
  return sound;
 }

 public String getBody() {
  return body;
 }

 public Data getData() {
  return DataObject;
 }

 public String getTo() {
  return to;
 }

 // Setter Methods 

 public void setTitle(String title) {
  this.title = title;
 }

 public void setBadge(String badge) {
  this.badge = badge;
 }

 public void setSound(String sound) {
  this.sound = sound;
 }

 public void setBody(String body) {
  this.body = body;
 }

 public void setData(Data dataObject) {
  this.DataObject = dataObject;
 }

 public void setTo(String to) {
  this.to = to;
 }
}


