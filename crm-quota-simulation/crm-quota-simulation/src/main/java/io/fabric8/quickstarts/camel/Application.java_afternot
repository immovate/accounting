/*
 * Copyright 2005-2016 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version
 * 2.0 (the "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
package io.fabric8.quickstarts.camel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.http.SSLContextParametersSecureProtocolSocketFactory;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.servlet.CamelHttpTransportServlet;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.camel.util.jsse.KeyManagersParameters;
import org.apache.camel.util.jsse.KeyStoreParameters;
import org.apache.camel.util.jsse.SSLContextParameters;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Component;


@SpringBootApplication
@ImportResource({"classpath:spring/camel-context.xml"})
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    

    @Bean
    ServletRegistrationBean servletRegistrationBean() {
        ServletRegistrationBean servlet = new ServletRegistrationBean(
            new CamelHttpTransportServlet(), "/crm-quota-sql/*");
        servlet.setName("CamelServlet");
        return servlet;
    }
    
    
    
   
    @Component
    class RestApi extends RouteBuilder {

        @Override
        public void configure() throws JSONException {
        	
        	
        	KeyStoreParameters ksp = new KeyStoreParameters();
        	ksp.setResource("key.jks"); 
        	ksp.setPassword("password");
        	KeyManagersParameters kmp = new KeyManagersParameters();
        	kmp.setKeyStore(ksp); 
        	kmp.setKeyPassword("password"); 
        	SSLContextParameters scp = new SSLContextParameters(); scp.setKeyManagers(kmp); 
        	ProtocolSocketFactory factory = new SSLContextParametersSecureProtocolSocketFactory(scp); 
        	Protocol.registerProtocol("https", new Protocol("https", factory, 443));
        	JacksonDataFormat prettyPrintDataFormat = new JacksonDataFormat(Connect.class);
            prettyPrintDataFormat.setUseList(true);//; setPrettyPrint(true);
           
        	restConfiguration()
                .contextPath("/crm-quota-sql").apiContextPath("/api-doc")
                    .apiProperty("api.title", "Camel REST API")
                    .apiProperty("api.version", "1.0")
                    .apiProperty("cors", "true")
                    .apiContextRouteId("doc-api")
                .component("servlet")
                .bindingMode(RestBindingMode.json);

            //subscribers details
            rest("/subid").description("Subscriber ID REST API")
                .get("/").description("List of subscribers")
	                .route().routeId("sub-api")
	                .to("sql:select * from subscribers?" +
	                    "dataSource=dataSource&" +
	                    "outputClass=io.fabric8.quickstarts.camel.Sub")
	                .endRest()
                .get("/{serialnumber}/{Email}").description("Subscriber's details")
	                .route().routeId("sub-api-serial")
	                .transform(simple("${random(1,30000)}"))
	                .to("sql:insert into subscribers (serial,email,secretcode) values " +
                    "(:#${header.serialnumber} , :#${header.Email}, :#${body})?" +
                    "dataSource=dataSource")
	                .setHeader("subject", constant("Secret Code"))
	                .setHeader("To", simple("${header.Email}"))
	                .to("smtps://smtp.gmail.com:465?password=Oa03001294@&username=atiaomar1978@gmail.com&From=acs@immovate.com")
                    .log("Inserted values ${body}")
                    .transform(simple("Secret code : ${body}"));
            
            
            
            
            //quota details
            rest("/").description("Quota REST API")
                .get("/delete/{serialnumber}/{secretcode}").description("List of quotas")
	                .route().routeId("delete-api")
	                .to("sql:delete from subscribers where serial = :#${header.serialnumber} and secretcode = :#${header.secretcode}?" +
	                    "dataSource=dataSource")
	                .to("sql:select * from subscribers where serial = :#${header.serialnumber} and secretcode = :#${header.secretcode}?" +
		                    "dataSource=dataSource&"+"outputType=SelectOne&"+"outputClass=io.fabric8.quickstarts.camel.code")
	                .log("Body count(*) ${header.CamelSqlRowCount} ${header.serialnumber}")             
	                .choice()
	                   .when(simple("${header.CamelSqlRowCount} == '0'"))
	                     .setBody(constant("result : Success"))
	                   .otherwise()
	                     .setBody(constant("result : Failed"))
	                   .log("Inserted values ${body}")
		               .endRest()
                .get("/secret/{serialnumber}/{secretcode}").description("Quota's details")
                .route().routeId("sec-api")
                .to("sql:select * from subscribers where serial = :#${header.serialnumber} and secretcode = :#${header.secretcode}?" +
                    "dataSource=dataSource&"+"outputType=SelectOne&"+"outputClass=io.fabric8.quickstarts.camel.code")
                .log("Body count(*) ${header.CamelSqlRowCount} ${header.serialnumber}")
                .choice()
                   .when(simple("${header.CamelSqlRowCount} == '1'"))
                     .setBody(constant("result : Success"))
                   .otherwise()
                     .setBody(constant("result : Failed"))
                   .log("Inserted values ${body}")
	                .endRest()
                .get("/login/{email}/{secretcode}").description("login api details")
                    .route().routeId("login-api")
                    .to("sql:select * from subscribers where email = :#${header.email} and secretcode = :#${header.secretcode}?" +
                        "dataSource=dataSource&"+"outputType=SelectOne&"+"outputClass=io.fabric8.quickstarts.camel.code")
                     .log("Body count(*) ${header.CamelSqlRowCount} ${header.email}")
                     .choice()
                         .when(simple("${header.CamelSqlRowCount} == '1'"))
                          .setBody(simple("serial : ${body.getserial}"))
                         .otherwise()
                          .setBody(constant("result : Login Failed"))
                         .log("Inserted values ${body}")
                         .endRest()
                .get("/AppID/{appid}/{secretcode}/{serial}").description("login api details")
                    .route().routeId("AppID-api")
           //         .to("sql:select * from AppDetails where serial = :#${header.serial}?" +
               //             "dataSource=dataSource&"+"outputType=SelectOne&"+"outputClass=io.fabric8.quickstarts.camel.appid")                   
          //          .log("Body count(*)   ${header.CamelSqlRowCount}")
             //       .convertBodyTo(String.class)
             //        .choice()
            //           .when(simple("${header.CamelSqlRowCount} == '0'"))
            //              .log("${header.appid} ${header.secretcode} ${header.serial}")
                    
                    .to("sql:insert into AppDetails (appid,secretcode,serial) values " +
                        "(:#${header.appid} , :#${header.secretcode}, :#${header.serial})?" +
                        "dataSource=dataSource")
                    .log("Body count(*) ${header.CamelSqlRowCount} ${header.email}")
                    .log("Inserted values ${body}")
                    .setBody(simple("Successfully inserted token : ${header.appid} aganist serial : ${header.serial} "))
            //           .otherwise()
               //     .setBody(constant("token is already registered!"))                 
                    .endRest()
                .post("/Push/{serial}").description("Push Notification")
                    .route().routeId("Push-api")
                    .log("${body}")
                    .marshal().json(JsonLibrary.Jackson,push.class)
                    .log("${body}")
                    .unmarshal().json(JsonLibrary.Jackson,push.class)
                    .log("${body}")
                    .setHeader("Push",simple("${body}"))
                 /*   .process(new Processor() {
                        public void process(Exchange exchange) throws Exception {
                            push payload = exchange.getIn().getBody(push.class);
                            log.info("payload."+payload);
                           exchange.getIn().setBody(payload);
                       }
                    })*/
                    .log("${body.getTitle} ${body.getBadge} ${body.getSound} ${body.getBody} ${body.getData.getMessage} ${header.serial} ${header.secretcode}")
                    
                    .to("sql:select * from AppDetails where serial = :#${header.serial}?" +
                            "dataSource=dataSource&"+"outputType=SelectList&"+"outputClass=io.fabric8.quickstarts.camel.appid")
                    
                    .log("Body count(*)   ${header.CamelSqlRowCount}")
                    .log("${body}")
                  // .log("${body[0].getappid}")
                    .setHeader("AppID",simple("${body}"))
                    .process(new Processor() {
                        public void process(Exchange exchange) throws Exception {
                        	ArrayList<push> hList = new ArrayList<push>();
//                        	ArrayList<appid> Appidlist = new ArrayList<appid>();
//                        	ArrayList<push> pusher = new ArrayList<push>();



                            push payload = (push)exchange.getIn().getHeader("Push");
                            
//                            Integer y = (Integer)exchange.getIn().getHeader("CamelSqlRowCount");
                        //    log.info("payload."+payload+"AppID"+Appid);
                            String app=null;
                            ArrayList<appid> Appid = (ArrayList<appid>) exchange.getIn().getHeader("AppID");
                            for (int i=0;i<Appid.size();i++)
                            { 
                            	push p = new push();
                            	p.setTitle(payload.getTitle());
                            	p.setBadge(payload.getBadge());
                            	p.setSound(payload.getSound());
                            	p.setBody(payload.getBody());
                            	p.setData(payload.getData());
                            	app =Appid.get(i).getappid();
                            	System.out.println("Appid: "+ app);
                            	p.setTo(app);
//                                payload.setTo(app);
                            	hList.add(p);
                            }
                           exchange.getIn().setBody(hList);
                       }
                    })
                   // .log("${body.getTitle} ${body.getTo} ${body.getBadge} ${body.getSound} ${body.getBody} ${body.getData.getMessage} ${header.serial} ${header.secretcode}") 
                    .log("${body}")
                    
                    .marshal().json(JsonLibrary.Jackson,push.class)
                    .log("${body}")
                    .removeHeaders("*")
                    .setHeader("Content-Type", constant("application/json"))
                    .setHeader("Accept", constant("application/json"))
                    .setHeader(Exchange.HTTP_METHOD, constant("POST")) 
                    .setHeader("CamelHttpUrl",constant("https://exp.host/--/api/v2/push/send"))
                    .setHeader("CamelServletContextPath",constant(""))
                    .setHeader("host",constant("exp.host"))
                    .setHeader("proto",constant("https"))
                    .setHeader("Host",constant("exp.host"))

                    .to("log:DEBUG?showBody=true&showHeaders=true")
                    .log("${body}")
                    .to("https://exp.host/--/api/v2/push/send"+"?bridgeEndpoint=true")
                    .convertBodyTo(String.class)
                    .setBody(simple("${body}"))
                    .endRest()
                 .get("/Delete/{serial}/{appid}").description("tsting details")
                    .route().routeId("Testing")
             //       .to("sql:select * from AppDetails where serial = :#${header.serial}?" +
             //               "dataSource=dataSource&"+"outputType=SelectList&"+"outputClass=io.fabric8.quickstarts.camel.appid")
             //       .transform(simple("${body[0].getappid} ${body[1].getappid}"));
                    .to("sql:delete from AppDetails where serial = :#${header.serial} and appid = :#${header.appid}?" +
    	                    "dataSource=dataSource")
                    .transform (simple("Commit Success : ${body}"))
                    .endRest()
                    
                    
                 .post("/ConnectedDevices/{serial}")
                    .description("Connected details").route().routeId("Connected Devices")
                    .marshal(prettyPrintDataFormat)
                    .log("${body}")
                    .convertBodyTo(String.class)
                //    .unmarshal(prettyPrintDataFormat)//.json(JsonLibrary.Jackson,Connect.class)
                //    .convertBodyTo(String.class)
                    .log("${body}")
                    .setHeader("Connect",simple("${body}"))
                    .to("sql:select * from ConnectedDevices where serial = :#${header.serial}?" +
                            "dataSource=dataSource&"+"outputType=SelectOne&"+"outputClass=io.fabric8.quickstarts.camel.QueryMac")
                    .log("Count(*) from serial"+"${header.CamelSqlRowCount}"+"   ${body}")
                 //   .log("header"+"${header.Connect}")
                    .process(new Processor() {
                        public void process(Exchange exchange) throws Exception {
//             	log.info("header inside"+exchange.getIn().getHeader("Connect"));
                        	
                       // 	String s = exchange.getIn().getBody(String.class);
                        
                       // 	log.info("Size"+n.size()+""+n.get(0).getResults().getName());
                            Integer count=(Integer)exchange.getIn().getHeader("CamelSqlRowCount");
                            List<String> DB_List = new ArrayList<String>();
                            if (count > 0)
                            {
                            	QueryMac QueryMacResult = (QueryMac) exchange.getIn().getBody();
                     
                                log.info("first Mac address"+QueryMacResult.getMac());
                                DB_List = Arrays.asList(QueryMacResult.getMac().split(","));
                            }
                        	
                      	   JSONObject Connecttest = new JSONObject((String)exchange.getIn().getHeader("Connect"));
                      	//   JSONObject Connecttest = new JSONObject(s);

                      	   log.info("Connect Test"+Connecttest);
                      	   JSONArray jo = Connecttest.getJSONArray("results");
                      	   
                      	  log.info("Connect jo"+jo);
//                             Integer y = (Integer)exchange.getIn().getHeader("CamelSqlRowCount");
                         //    log.info("payload."+payload+"AppID"+Appid);
                      	   
                      	  StringBuilder mac=new StringBuilder();
                          List<String> ACS_List = new ArrayList<String>();
                             for (int i=0;i<jo.length();i++)
                             { 
                             	JSONObject jj = new JSONObject(""+jo.get(i));
                          	   ConnectedDevice cd = new ConnectedDevice();
                          	   cd.setName(jj.get("name").toString());
                          	   cd.setMacaddress(jj.get("macaddress").toString());
                             	mac.append(cd.getMacaddress());
                             	if (i!=jo.length()-1)
                             	mac.append(",");
                           
                             }
                             ACS_List = Arrays.asList(new String(mac).split(","));
                        	 log.info("mac"+jo);
                            
                            
                            exchange.getIn().setBody(mac);
                            exchange.getIn().setHeader("mac", mac);
                            
                            
                           if (!DB_List.isEmpty()) {
                            StringBuilder newConnectedDevices=new StringBuilder();
                            for(int k=0; k<ACS_List.size();k++) {
                            	if(!DB_List.contains(ACS_List.get(k))) {
                            		newConnectedDevices.append(ACS_List.get(k));
                            		if(k!=ACS_List.size()-2){
                            			newConnectedDevices.append(",");
                            		}
                            	}
                            }
                            log.info("New Connected Devices: " + newConnectedDevices);
                            if(newConnectedDevices.length()>0 || (ACS_List.size()!=DB_List.size()))
                            {
                            	exchange.getIn().setHeader("ShouldUpdate", "true");
                            	if(newConnectedDevices.length()>0) {
                                	exchange.getIn().setHeader("NewConnctedDevice", "true");
                                	exchange.getIn().setHeader("NewMacs",newConnectedDevices);

                            	}
                            }
                           }
                        }
                     })
                     .choice()
                       .when(simple("${header.CamelSqlRowCount} == '0' "))
                           .log("I'm inserting into DB")
                           .to("sql:insert into ConnectedDevices (serial,mac) values " +
                             "(:#${header.serial} , :#${header.mac})?" +
                             "dataSource=dataSource")
                           .setBody(constant("Success Inserted"))
                       .when(simple("${header.ShouldUpdate} == 'true' "))
                       	   .to("sql:update ConnectedDevices set mac = :#${header.mac} where serial = :#${header.serial}" +
                               "?" +
                               "dataSource=dataSource&"+"outputType=SelectOne")
                           .log("I'm in otherwise after update..count(*) ${header.CamelSqlUpdateCount}")
                           .setBody(constant("Number of Connected Devices has been changed..."))
                         .choice()
                          .when(simple("${header.NewConnctedDevice} == 'true' "))
                           .to("sql:select * from AppDetails where serial = :#${header.serial}?" +
                               "dataSource=dataSource&"+"outputType=SelectList&"+"outputClass=io.fabric8.quickstarts.camel.appid")
                       
                           .log("Body count(*)   ${header.CamelSqlRowCount}")
                           .setHeader("AppID",simple("${body}"))
                              .choice()
                                .when(simple("${header.CamelSqlRowCount} > '0' ")) 
	                             .process(new Processor() { public void process(Exchange exchange) throws Exception {
	                             	ArrayList<push> hList = new ArrayList<push>();
	
	                           
	//                         Integer y = (Integer)exchange.getIn().getHeader("CamelSqlRowCount");
	                     //    log.info("payload."+payload+"AppID"+Appid);
		                                 String app=null;
		                                 ArrayList<appid> Appid = (ArrayList<appid>) exchange.getIn().getHeader("AppID");
		                              for (int i=0;i<Appid.size();i++)
		                             { 
		                         	push p = new push();
		                         	p.setTitle("New Connected Devices");
		                         	p.setBadge("1");
		                         	p.setSound("default");
		                         	p.setBody("You have new Connected Devices");
		                         	Data message = new Data();
		                         	message.setMessage(""+exchange.getIn().getHeader("NewMacs"));
		                         	p.setData(message);
		                         	app =Appid.get(i).getappid();
		                         	System.out.println("Appid: "+ app);
		                         	p.setTo(app);
		//                             payload.setTo(app);
		                         	hList.add(p);
		                           }
		                           exchange.getIn().setBody(hList);
		                    
		                       
		                       
		                          }})
		                          .log("${body}")
		                       
		                          .marshal().json(JsonLibrary.Jackson,push.class)
		                          .log("${body}")
		                          .removeHeaders("*")
		                          .setHeader("Content-Type", constant("application/json"))
		                          .setHeader("Accept", constant("application/json"))
		                          .setHeader(Exchange.HTTP_METHOD, constant("POST")) 
		                          .setHeader("CamelHttpUrl",constant("https://exp.host/--/api/v2/push/send"))
		                          .setHeader("CamelServletContextPath",constant(""))
		                          .setHeader("host",constant("exp.host"))
		                          .setHeader("proto",constant("https"))
		                          .setHeader("Host",constant("exp.host"))
		                          .to("log:DEBUG?showBody=true&showHeaders=true")
		                          .log("${body}")
		                          .to("https://exp.host/--/api/v2/push/send"+"?bridgeEndpoint=true")
		                          .convertBodyTo(String.class)
		                          .setBody(simple("${body}"))
		                      .otherwise()
		                          .setBody(constant("Your serial doesn't have token or AppID Please relogin again"))
		                 .endChoice()
                         .otherwise()
                          .setBody(constant("I didn't send message"))
                       .endChoice()
                       .otherwise()
                        .setBody(constant("No Newly Connected Devices"));
;
                   
                   
           //   

            
            
            
            
            
            
            
            
            
        }
    }
}